# Traffic Drones - Java Test

## Requirements

- Java 1.8
- Maven 3

## Setup the application

Execute the next commands to run the application in the folder where the repository has been cloned:
> mvn spring-boot:run

The files with routes info should be on src/main/resources/ with the format "<dronId>.csv"
The dron list to deploy should be configurated on src/main/resources/application.yml

When a dron receive a new order, the dron prints the message "Dron {dronId} received Coordinates {order}"
When the dron detect a nerby station, the dron prints "Dron {dronId} is {currentPosition} and has detected near {station}"
When the dispatcher receive a traffic report, the dispatcher prints "TrafficReport received: {trafficReport}"