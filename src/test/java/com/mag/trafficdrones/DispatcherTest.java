package com.mag.trafficdrones;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import com.mag.trafficdrones.pojo.Coordinates;

@RunWith(MockitoJUnitRunner.class)
public class DispatcherTest {
	private Dispatcher dispatcher;
	
	@Mock
	private ApplicationEventPublisher publisher;
	
	@Before
	public void setup() {
		dispatcher = new Dispatcher(Arrays.asList("5937"), publisher);
	}
	
	@Test
	public void shouldSendEvent() throws Exception {
		dispatcher.run();
		Coordinates expectedCoor = new Coordinates("5937,\"51.476105\",\"-0.100224\",\"2011-03-22 07:55:26\"");
		verify(publisher, times(1)).publishEvent(expectedCoor);
	}
}
