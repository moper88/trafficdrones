package com.mag.trafficdrones.pojo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.mag.trafficdrones.pojo.Coordinates;

public class CoordinatesTest {

	@Test
	public void shouldConstructCoordinatesFromLine() {
		Coordinates coor = new Coordinates("5937,\"51.476105\",\"-0.100224\",\"2011-03-22 07:55:26\"");
		
		assertNotNull(coor);
		assertEquals(5937, coor.getDronId());
		assertEquals(51.476105, coor.getPoint().getLatitude(), 0.000001);
		assertEquals(-0.100224, coor.getPoint().getLongitude(), 0.000001);
		assertEquals("2011-03-22T07:55:26", coor.getTime().toString());
	}
}
