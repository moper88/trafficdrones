package com.mag.trafficdrones.pojo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.mag.trafficdrones.pojo.TubeStation;

public class TubeStationTest {
	@Test
	public void shouldConstructTubeStationTestFromLine() {
		TubeStation tubeStation = new TubeStation("\"Acton Town\",51.503071,-0.280303");
		
		assertNotNull(tubeStation);
		assertEquals("Acton Town", tubeStation.getName());
		assertEquals(51.503071, tubeStation.getPoint().getLatitude(), 0.000001);
		assertEquals(-0.280303, tubeStation.getPoint().getLongitude(), 0.000001);
	}
}
