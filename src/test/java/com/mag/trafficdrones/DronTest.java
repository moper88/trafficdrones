package com.mag.trafficdrones;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import com.mag.trafficdrones.pojo.Coordinates;
import com.mag.trafficdrones.pojo.TrafficReport;
import com.mag.trafficdrones.pojo.TubeStation;

@RunWith(MockitoJUnitRunner.class)
public class DronTest {

	private Dron dron;
	
	@Mock
	private ApplicationEventPublisher publisher;
	
	@Before
	public void setup() {
		dron = new Dron(5937,Arrays.asList(new TubeStation("\"Acton Town\",51.503071,-0.280303")), publisher);
	}
	
	@Test
	public void shouldSendEvent() {
		dron.move(new Coordinates("5937,\"51.503072\",\"-0.280304\",\"2011-03-22 07:55:26\""));
		verify(publisher, times(1)).publishEvent(any(TrafficReport.class));
	}
	
	@Test
	public void shouldNotSendEventBecauseIsTooFar() {
		dron.move(new Coordinates("5937,\"51.603072\",\"-0.280304\",\"2011-03-22 07:55:26\""));
		verify(publisher, times(0)).publishEvent(any(TrafficReport.class));
	}
	
	@Test
	public void shouldNotHandleEventForOtherDron() {
		dron.move(new Coordinates("6043,\"51.503072\",\"-0.280304\",\"2011-03-22 07:55:26\""));
		verify(publisher, times(0)).publishEvent(any(TrafficReport.class));
	}
}
