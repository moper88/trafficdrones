package com.mag.trafficdrones;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.mag.trafficdrones.pojo.Coordinates;
import com.mag.trafficdrones.pojo.TrafficReport;

@Component
public class Dispatcher implements CommandLineRunner{
	
	private static final Logger logger = LoggerFactory.getLogger(Dispatcher.class);
	
	private final ApplicationEventPublisher publisher;
	
	private List<String> dronIds;
	
	public Dispatcher(List<String> dronIds, ApplicationEventPublisher publisher) {
		this.dronIds = dronIds;
		this.publisher = publisher;
		logger.info("Dispatcher created");
	}
	
	@Override
	public void run(String... args) throws Exception {
		logger.info("Running Dispacher");
		for (String dronId : dronIds) {
			sendMovesToDron(dronId);
		}
	}
	
	private void sendMovesToDron(String dronId) {
		getCoordinatesFromFile(dronId).forEach(publisher::publishEvent);
	}
	
	private List<Coordinates> getCoordinatesFromFile(String dronId) {
		String filename = dronId +".csv";
		logger.info("Reading file {}", filename);
		try (FileReader fileReader = new FileReader(new ClassPathResource(filename).getFile());
				BufferedReader bufferedReader = new BufferedReader(fileReader)) {
			return bufferedReader.lines().map(Coordinates::new).collect(Collectors.toList());
		} catch (IOException e) {
			logger.error("Error reading coordinates from file {}", filename, e);
			return new ArrayList<>();
		}
	}
	
	@EventListener
	public void handleTrafficReport(TrafficReport report) {
		logger.info("TrafficReport received: {}", report);
	}

}
