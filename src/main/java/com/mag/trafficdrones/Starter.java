package com.mag.trafficdrones;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.mag.trafficdrones.pojo.TubeStation;

@SpringBootApplication
@EnableJms
@EnableAsync
public class Starter {
	
	public static void main(String[] args) {
		SpringApplication.run(Starter.class, args);
	}
	
	@Bean
	public List<TubeStation> tubeStations() throws IOException {
		
		try (FileReader fileReader = new FileReader(new ClassPathResource("tube.csv").getFile());
				BufferedReader bufferedReader = new BufferedReader(fileReader);) {
			return bufferedReader.lines().map(TubeStation::new).collect(Collectors.toList());
		}
		
	}
	
	@Bean
	public DronFactoryPostProcessor dronFactoryPostProcessor(@Value("#{'${dronIds}'.split(',')}") List<String> dronIds, ApplicationEventPublisher publisher, List<TubeStation> tubeStations) {
		return new DronFactoryPostProcessor(dronIds, tubeStations, publisher);
	}
	
	@Bean
	public Dispatcher dispatcher(@Value("#{'${dronIds}'.split(',')}") List<String> dronIds, ApplicationEventPublisher publisher) {
		return new Dispatcher(dronIds, publisher);
	}
	
	@Bean
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("DronThread-");
		executor.initialize();
		return executor;
	}
}
