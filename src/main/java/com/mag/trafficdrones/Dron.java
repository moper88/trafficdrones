package com.mag.trafficdrones;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;

import com.mag.trafficdrones.pojo.Coordinates;
import com.mag.trafficdrones.pojo.Point;
import com.mag.trafficdrones.pojo.TrafficCondition;
import com.mag.trafficdrones.pojo.TrafficReport;
import com.mag.trafficdrones.pojo.TubeStation;

public class Dron {
	
	private static final Logger logger = LoggerFactory.getLogger(Dron.class);
	
	private final int id;
	
	private List<TubeStation> tubeStations;
	
	private final ApplicationEventPublisher publisher;
	
	private Coordinates lastCoordinate;
	
	public Dron(Integer id, List<TubeStation> tubeStations, ApplicationEventPublisher publisher) {
		super();
		this.id = id;
		this.tubeStations = tubeStations;
		this.publisher = publisher;
		this.lastCoordinate = null;
		logger.info("Dron {} created", id);
	}
	
	@EventListener
	public void move(Coordinates coordinates) {
		if (belongsToMe(coordinates)) {
			logger.info("Dron {} received Coordinates {}",id, coordinates);
			Point coordinatesPoint = coordinates.getPoint();
			tubeStations.stream().filter(tubeStation -> isNearby(coordinatesPoint, tubeStation))
					.forEach(tubeStation -> generateTrafficReport(tubeStation, coordinates));
			lastCoordinate = coordinates;
		}
	}
	
	private void generateTrafficReport(TubeStation tubeStation, Coordinates coordinates) {
		logger.info("Dron {} is {} and has detected near {}", id, coordinates, tubeStation);
		
		publisher.publishEvent(new TrafficReport(id, Instant.now(), speed(coordinates), TrafficCondition.random()));
	}

	private double speed(Coordinates coordinates) {
		if (coordinates == null || coordinates.getPoint() == null || coordinates.getTime() == null
				|| lastCoordinate == null || lastCoordinate.getPoint() == null || lastCoordinate.getTime() == null) {
			return 0;
		}
		double distance = coordinates.getPoint().distanceTo(lastCoordinate.getPoint());
		double time = Duration.between(coordinates.getTime(), lastCoordinate.getTime()).getSeconds();
		return Math.abs(distance / time);
	}

	private boolean isNearby(Point coordinatesPoint, TubeStation tubeStation) {
		if (coordinatesPoint == null || tubeStation == null || tubeStation.getPoint() == null) {
			return false;
		}
		return coordinatesPoint.distanceTo(tubeStation.getPoint()) < 350;
	}

	private boolean belongsToMe(Coordinates coordinates) {
		return id == coordinates.getDronId();
	}

	public int getId() {
		return id;
	}
}
