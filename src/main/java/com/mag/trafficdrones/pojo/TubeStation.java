package com.mag.trafficdrones.pojo;

import org.springframework.util.StringUtils;

public class TubeStation {
	private String name;
	private Point point;
	
	public TubeStation(String line) {
		if (!StringUtils.isEmpty(line)) {
			String[] fields = line.split(",");
			this.name = fields[0].substring(1, fields[0].length() -1);
			this.point = new Point(Double.valueOf(fields[1]), Double.valueOf(fields[2]));
		}
	}
	
	public TubeStation(String name, double latitude, double longitude) {
		super();
		this.name = name;
		this.point = new Point(latitude, longitude);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Point getPoint() {
		return point;
	}
	public void setPoint(Point point) {
		this.point = point;
	}
	
}
