package com.mag.trafficdrones.pojo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.util.StringUtils;

public class Coordinates {
	private int dronId;
	private Point point;
	private LocalDateTime time;
	
	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	public Coordinates(String line) {
		if (!StringUtils.isEmpty(line)) {
			String[] fields = line.split(",");
			this.dronId = Integer.valueOf(fields[0]);
			this.point = new Point(Double.valueOf(deleteFirstAndLastChar(fields[1])),
								Double.valueOf(deleteFirstAndLastChar(fields[2])));
			this.time = LocalDateTime.parse(deleteFirstAndLastChar(fields[3]), formatter);
		}
	}
	
	private String deleteFirstAndLastChar(String string) {
		return string.substring(1, string.length() -1);
	}

	public int getDronId() {
		return dronId;
	}
	public void setDronId(int dronId) {
		this.dronId = dronId;
	}
	public Point getPoint() {
		return point;
	}
	public void setPoint(Point point) {
		this.point = point;
	}
	public LocalDateTime getTime() {
		return time;
	}
	public void setTime(LocalDateTime time) {
		this.time = time;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dronId;
		result = prime * result + ((point == null) ? 0 : point.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinates other = (Coordinates) obj;
		if (dronId != other.dronId)
			return false;
		if (point == null) {
			if (other.point != null)
				return false;
		} else if (!point.equals(other.point))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Coordinates [dronId=" + dronId + ", point=" + point + ", time="
				+ time + "]";
	}
	
}
