package com.mag.trafficdrones.pojo;

import java.time.Instant;

public class TrafficReport {
	private int dronId;
	private Instant time;
	private double speed;
	private TrafficCondition trafficCondition;
	
	
	public TrafficReport(int dronId, Instant time, double speed, TrafficCondition trafficCondition) {
		super();
		this.dronId = dronId;
		this.time = time;
		this.speed = speed;
		this.trafficCondition = trafficCondition;
	}
	
	public int getDronId() {
		return dronId;
	}
	public void setDronId(int dronId) {
		this.dronId = dronId;
	}
	public Instant getTime() {
		return time;
	}
	public void setTime(Instant time) {
		this.time = time;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public TrafficCondition getTrafficCondition() {
		return trafficCondition;
	}
	public void setTrafficCondition(TrafficCondition trafficCondition) {
		this.trafficCondition = trafficCondition;
	}
	@Override
	public String toString() {
		return "TrafficReport [dronId=" + dronId + ", time=" + time + ", speed=" + speed + ", trafficCondition="
				+ trafficCondition + "]";
	}
	
	
}
