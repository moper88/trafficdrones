package com.mag.trafficdrones;

import java.util.List;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.mag.trafficdrones.pojo.TubeStation;

@Component
public class DronFactoryPostProcessor implements BeanFactoryPostProcessor {
	
	private List<String> dronIds;

	private ApplicationEventPublisher publisher;

	
	private List<TubeStation> tubeStations;
	
	public DronFactoryPostProcessor(List<String> dronIds, List<TubeStation> tubeStations, ApplicationEventPublisher publisher) {
		super();
		this.dronIds = dronIds;
		this.tubeStations = tubeStations;
		this.publisher = publisher;
	}
	
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) {
		for (String dronId : dronIds) {
			createAndRegister((BeanDefinitionRegistry) beanFactory, dronId);
		}
	}

	private void createAndRegister(BeanDefinitionRegistry registry, String dronId) {
		Integer dronIdInt = Integer.valueOf(dronId);
		registry.registerBeanDefinition("dron" + dronId, BeanDefinitionBuilder
				.genericBeanDefinition(Dron.class)
				.addConstructorArgValue(dronIdInt)
				.addConstructorArgValue(tubeStations)
				.addConstructorArgValue(publisher)
				.getBeanDefinition());
	}
}
